module NotesHelper
  def show_notes(note)  
    if request.path == '/shared'
      render 'notes', note: note if note.user != current_user
    elsif request.path == '/mystickys'
      render 'notes', note: note if note.user == current_user
    else 
      render 'notes', note: note 
    end
  end
end
