jQuery(function() {
  return $('#post_publish_date').datepicker({
    dateFormat: 'yy-mm-dd'
  });
});


$(function() {
   $("#post_tags").tokenInput([
     {id: 72, name: "Mystery"},
     {id: 11, name: "Comedy"},
     {id: 13, name: "Adventure"},
     {id: 17, name: "ActionScript"},
     {id: 19, name: "Scheme"},
     {id: 23, name: "Lisp"},
     {id: 29, name: "C#"},
     {id: 31, name: "Fortran"},
     {id: 37, name: "Visual Basic"},
     {id: 41, name: "C"},
     {id: 43, name: "C++"}, 
     {id: 47, name: "Java"}],
     {theme: "scrippet",
      preventDuplicates: true});
 });
