# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).

puts "ROLES"
YAML.load(ENV['ROLES']).each do |role|
    Role.find_or_create_by(name: role)
    puts 'role: ' << role
end

puts "DEFAULT USERS"
user = User.find_or_create_by(email: ENV['ADMIN_EMAIL'].dup) do |user|
    user.name = ENV['ADMIN_NAME'].dup
    user.password = ENV['ADMIN_PASSWORD'].dup
    user.password_confirmation = ENV['ADMIN_PASSWORD'].dup
    user.add_role :admin
    puts user.name
end
user = User.find_or_create_by(email: 'test@user.com') do |user|
    user.name = 'Test User'
    user.password = 'password'
    user.password_confirmation = 'password'
    puts user.name
end

puts 'SCRIPPET CATEGORIES'
categories = Category.create([{ name: 'Book & Story Scrippets' }, { name: 'Play Scrippets' }, { name: 'Film Scrippets' }, { name: 'Song Scrippets' }, { name: 'Community Scrippets' }]) 
