class CreatePosts < ActiveRecord::Migration
  def change
    create_table :posts do |t|
      t.string :title
      t.text :body
      t.date :publish_date
      t.integer :user_id
      t.boolean :adult
      t.boolean :terms

      t.timestamps
    end
  end
end
