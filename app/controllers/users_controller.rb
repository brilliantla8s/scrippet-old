class UsersController < ApplicationController
  before_filter :authenticate_user!
 

  def follow
    @user = User.find(id_params[:id])
    if current_user == @user
      redirect_to users_path, :alert => "You cannot follow yourself."
    else
      current_user.follow(@user)
    # RecommenderMailer.new_follower(@user).deliver if @user.notify_new_follower
      redirect_to followers_path, :notice => "You are now following #{@user.name}."
    end
  end

  def unfollow
    @user = User.find(id_params[:id])
    current_user.stop_following(@user)
    redirect_to users_path, :notice => "You are no longer following #{@user.name}."
  end

  def block
    @user = User.find(id_params[:id])
    if current_user == @user
      redirect_to users_path, :alert => "You cannot block yourself."
    else
      current_user.block(@user)
      redirect_to users_path, :notice => "You have blocked #{@user.name}"
    end
  end

  def unblock
    @user = User.find(id_params[:id])
    current_user.unblock(@user)
    redirect_to users_path, :notice => "You have unblocked #{@user.name}"
  end

  def index
    if request.path == following_path
      @users = current_user.all_following
    elsif request.path == followers_path
      @users = current_user.user_followers
    else
      @users = User.all
      #authorize! :index, @user, :message => 'Not authorized as an administrator.'
    end
  end

  def show
      @user = User.find(id_params[:id])
      @posts = Post.by_user_id(params[:id]).limit(3).order('publish_date DESC')
  end
  
  def update
    authorize! :update, @user, :message => 'Not authorized as an administrator.'
    @user = User.find(id_params)
    role = Role.find(params[:user][:role_ids]) unless params[:user][:role_ids].nil?
    params[:user] = params[:user].except(:role_ids)
    
   if @user.update_attributes(id_params)
      @user.update_plan(role) unless role.nil?
      redirect_to users_path, :notice => "User updated."
    else
      redirect_to users_path, :alert => "Unable to update user."
    end
  end
    
  def destroy
    authorize! :destroy, @user, :message => 'Not authorized as an administrator.'
    user = User.find(id_params)
    
    unless user == current_user
      user.destroy
      redirect_to users_path, :notice => "User deleted."
    else
      redirect_to users_path, :notice => "Can't delete yourself."
    end
  end
  
  private

  
  def id_params
      params.permit(:id, :name, :email, :password, :password_confirmation, :remember_me, :stripe_token, :coupon, :followable, :follower, :blocked)
  end

end
