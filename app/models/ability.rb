class Ability
  include CanCan::Ability

  def initialize(user)
    user ||= User.new # guest user (not logged in)
    can :read, Post
    unless user.new_record?
       can :create, Post
       can :update, Post, :user_id => user.id
       can :destroy, Post, :user_id => user.id
    end
    if user.has_role? :admin
      can :manage, :all
    else
      can :view, :scripper if user.has_role? :scripper
      can :view, :scrippeter if user.has_role? :scrippeter
      can :view, :scripperator if user.has_role? :scripperator
    end    
   end
end
