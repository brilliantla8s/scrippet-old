class Note < ActiveRecord::Base
  validates :title, presence: true
  belongs_to :user
  has_many :notes_accessors, dependent: :destroy
end
