class ApplicationController < ActionController::Base
  protect_from_forgery
  before_filter :update_sanitized_params, if: :devise_controller?
  before_filter :load_schema, :authenticate_user_from_token!, :cancan_rails4_hack
  
  def cancan_rails4_hack
    resource = controller_name.singularize.to_sym
    method = "#{resource}_params"
    params[resource] &&= send(method) if respond_to?(method, true)
  end

  @categories = Category.all
    
  rescue_from CanCan::AccessDenied do |exception|
    redirect_to root_path, :alert => exception.message
  end

  def after_sign_in_path_for(resource)
    posts_path
  end

  def after_sign_out_path_for(resource_or_scope)
    root_path
  end
  
private
   
   def authenticate_user_from_token!
    e = params[:e].presence
    user = e && User.find_by_email(e)
    if user && Devise.secure_compare(user.authentication_token, params[:t])
      if user.sign_in_count == 0
        sign_in user, store: true
      else
        sign_in user, store: false
      end
    end
  end

  def load_schema
    Apartment::Database.switch('public')
    return unless request.subdomain.present?

    tenant = Tenant.find_by(subdomain: request.subdomain)
    if tenant
      Apartment::Database.switch(tenant.subdomain)
    else
      redirect_to root_url(subdomain: false)
    end
  end

  private
  # Rails 4 Documentation: https://github.com/plataformatec/devise#strong-parameters
  def update_sanitized_params
    devise_parameter_sanitizer.for(:sign_in) {|u| u.permit(:email, :password)}
    devise_parameter_sanitizer.for(:sign_up) {|u| u.permit(:name, :email, :password, :password_confirmation)}
    devise_parameter_sanitizer.for(:user_update) {|u| u.permit(:name, :email, :password, :password_confirmation, :current_password)}
  end  
end
