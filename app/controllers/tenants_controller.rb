class TenantsController < ApplicationController
  skip_before_filter :authenticate_user!, only: [:new, :create]

  def new
    @plan = params[:plan]    
    if @plan && ENV["ROLES"].include?(@plan) && @plan != "admin"
      @tenant = Tenant.new
      @tenant.build_account
    else
      redirect_to root_path, notice: 'Please select a subscription plan below.'
    end
  end

  def create
    @tenant = Tenant.new(account_params)   
    if @tenant.valid? 
      Apartment::Database.create(@tenant.subdomain)
      Apartment::Database.switch(@tenant.subdomain)
      @tenant.account.add_role(params[:plan])
      @tenant.save  
      redirect_to welcome_url(subdomain: @tenant.subdomain, e: @tenant.account.email, t: @tenant.account.authentication_token)
    else
      render action: 'new'
    end
  end 

private
  def account_params
    params.require(:tenant).permit(:subdomain, account_attributes: [:name, :email, :password, :password_confirmation, :stripe_token, :coupon])
  end
end


