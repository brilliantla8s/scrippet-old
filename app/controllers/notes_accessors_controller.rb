class NotesAccessorsController < ApplicationController
  before_action :set_notes_accessor, only: [:show, :edit, :update, :destroy]
  before_filter :authenticate_user!

  # GET /notes_accessors
  def index
    @notes_accessors = NotesAccessor.where("access_owner = ?" , current_user.id)
    @notes_accessors_by_others = NotesAccessor.where("notes_accessor_id = ?" , current_user.id)
  end

  # GET /notse_accessors/new
  def new
    @notes_accessor = NotesAccessor.new
    @note_id = params[:note_id]
    if Note.find(params[:note_id]).user_id != current_user.id
      render :file => 'public/404.html'
    end
  end

  # POST /notes_accessors
  def create
    idForNote = params[:notes_accessor][:note_id]
    idForNoteAccessor = User.where(email: params[:notes_accessor][:notes_accessor_id]).first.id
    @notes_accessor = NotesAccessor.find_or_initialize_by_note_id_and_notes_accessor_id_and_access_owner(idForNote, idForNoteAccessor , current_user.id)
    @notes_accessor.update_attributes(
        :access_rights => params[:notes_accessor][:access_rights]
    )
    
    if @notes_accessor.save
      redirect_to notes_accessors_url, notice: 'Note accessor was successfully created.'
    else
      render action: 'new'
    end
  end

  # DELETE /notes_accessors/1
  def destroy
    @notes_accessor.destroy
    redirect_to notes_accessors_url, notice: 'Note accessor was successfully destroyed.'
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_notes_accessor
      @notes_accessor = NotesAccessor.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def accessor_params
      params.require(:notes_accessor).permit(:notes_accessor_id, :access_rights, :note_id)
    end
end
