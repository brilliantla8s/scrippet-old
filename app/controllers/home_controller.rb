class HomeController < ApplicationController

  def join
  end

  def welcome
  end

  def dashboard
  end

  def index
    @posts = Post.order('publish_date DESC').limit(1)
  end

end
