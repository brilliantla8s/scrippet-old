class Post < ActiveRecord::Base
  acts_as_taggable
  validates_presence_of :title, :body, :publish_date, :user_id 
 # validates :terms, acceptance: true
  belongs_to :user
  has_and_belongs_to_many :categories
  scope :published, lambda { where(['publish_date <= ?', Date.today]) }
  scope :by_user_id, lambda {|uid| where(:user_id => uid)}
  scope :by_category_id, lambda {|cid| joins(:categories).where(['categories.id =?',cid])}
 
  include PgSearch
  pg_search_scope :search, against: [:title, :body],
  using: {tsearch: {dictionary: "english"}},
 # associated_against: {author: :name, comments: [:name, :content]},
  ignoring: :accents

  def self.text_search(query)
  if query.present?
    rank = <<-RANK
      ts_rank(to_tsvector(title), plainto_tsquery(#{sanitize(query)})) +
      ts_rank(to_tsvector(body), plainto_tsquery(#{sanitize(query)}))
    RANK
    where("to_tsvector('english', title) @@ :q or to_tsvector('english', body) @@ :q", q: query).order("#{rank} desc")
  else
    scoped
  end
end

end




