class SubdomainPresent
  def self.matches?(request)
    request.subdomain.present?
  end
end

class SubdomainBlank
  def self.matches?(request)
    request.subdomain.blank?
  end
end

Scrippet::Application.routes.draw do 
  mount StripeEvent::Engine => '/stripe'
  resources :notes_accessors, path: 'permissions'
  resources :notes, path: 'stickys'
  resources :projects
  get 'mystickys' => 'notes#index'
  get 'shared' => 'notes#index'
  get 'dashboard' => 'home#dashboard'
  get 'messages' => 'home#plans'
  resources :categories
  resources :posts, path: 'scrippets'
  get 'tags/:tag', to: 'posts#index', as: :tag  
  devise_for :users, controllers: { :registrations => 'registrations' }, path: 'scrippers'
  devise_scope :user do
    put 'update_plan', :to => 'registrations#update_plan'
    put 'update_card', :to => 'registrations#update_card'
  end 
  resources :users, path: 'scrippers' do
    member do
      get :follow, :unfollow, :block, :unblock    
    end
  end
  get 'followers' => 'users#index'
  get 'following' => 'users#index'

  constraints(SubdomainPresent) do
    get 'welcome' => 'home#welcome'
    root 'home#index', as: :subdomain_root
  end

  constraints(SubdomainBlank) do   
    get 'join' => 'home#join' 
    resources :tenants, only: [:new, :create] 
    root 'home#index'   
  end 
end



