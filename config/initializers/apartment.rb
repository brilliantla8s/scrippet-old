#
# Apartment Configuration
#
Apartment.configure do |config|

  # these models will not be multi-tenanted,
  # but remain in the global (public) namespace
  config.excluded_models = ['Tenant']

  # use postgres schemas?
  config.use_schemas = true
  config.seed_after_create = true
  # config.default_schema = 'schema_name'

  # supply list of database names for migrations to run on
  # database_names was deprecated, switched to tenant_names
  config.tenant_names = -> { Tenant.pluck(:subdomain) }

  # Use a static list of database names for migrate
  # config.tenant_names = ['db1', 'db2']
  # config.persistent_schemas = ['pubic']
end

##
# Elevator Configuration

# Rails.application.config.middleware.use 'Apartment::Elevators::Generic', lambda { |request|
#   # TODO: supply generic implementation
# }

Rails.application.config.middleware.use 'Apartment::Elevators::Subdomain'

# Excluded Subdomains
Apartment::Elevators::Subdomain.excluded_subdomains = ['www', 'public', 'home']
