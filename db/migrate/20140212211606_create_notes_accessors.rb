class CreateNotesAccessors < ActiveRecord::Migration
  def change
    create_table :notes_accessors do |t|
      t.integer :note_id
      t.integer :notes_accessor_id
      t.boolean :access_rights
      t.integer :access_owner

      t.timestamps
    end
  end
end
