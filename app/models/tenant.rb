class Tenant < ActiveRecord::Base
  
  RESTRICTED_SUBDOMAINS = %w(www)

  belongs_to :account, class_name: 'User'

  validates :account, presence: true
  validates :subdomain, presence: true,
                        uniqueness: { case_sensitive: false },
                        format: { with: /\A[\w\-]+\Z/i, message: 'contains invalid characters' },
                        exclusion: { in: RESTRICTED_SUBDOMAINS, message: 'restricted' }

  accepts_nested_attributes_for :account
  before_validation :downcase_subdomain
  before_save :update_stripe
  before_destroy :cancel_subscription

private 
  def downcase_subdomain
    self.subdomain = subdomain.try(:downcase)
  end 
  
  def update_stripe
    return if account.email.include?(ENV['ADMIN_EMAIL'])
    return if account.email.include?('@test.com') and not Rails.env.production?
    return if subdomain.nil?
    if !account.stripe_token.present?
      raise "There was an issue while trying to create your account"
    end
    if account.coupon.blank?
      customer = Stripe::Customer.create(
        :email => account.email,
        :description => account.name,
        :card => account.stripe_token,
        :plan => account.roles.first.name
      )
    else
      customer = Stripe::Customer.create(
        :email => account.email,
        :description => account.name,
        :card => account.stripe_token,
        :plan => account.roles.first.name,
        :coupon => account.coupon
      )
    end
    self.last_4_digits = customer.cards.data.first["last4"]
    self.customer_id = customer.id
    self.account.update_column(:last_4_digits, customer.cards.data.first["last4"])
    self.account.update_column(:customer_id, customer.id)
    self.account.stripe_token = nil
  rescue Stripe::StripeError => e
    logger.error "Stripe Error: " + e.message
    errors.add :base, "#{e.message}."
    account.stripe_token = nil
    false
  end    
  
end
