class CreateTenants < ActiveRecord::Migration
  def change
    create_table :tenants do |t|
      t.integer :account_id
      t.string :subdomain

      ## stripe
      t.string :customer_id
      t.string :last_4_digits

      t.timestamps
    end
  end
end
