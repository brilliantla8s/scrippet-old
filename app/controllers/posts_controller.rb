class PostsController < ApplicationController
  before_action :set_post, only: [:show, :edit, :update, :destroy]
  before_filter :authenticate_user!, :except => :index
 # load_and_authorize_resource 

  # GET /posts
  def index
    @posts = Post.order('publish_date DESC')
    @posts = @posts.published if current_user.blank?
    @posts = @posts.by_user_id(params[:user]) if params[:user].present?
    @posts = @posts.by_category_id(params[:cat]) if params[:cat].present?
    @posts = @posts.tagged_with(params[:tag]) if params[:tag].present?
    @posts = @posts.text_search(params[:query]).page(params[:page])#.per_page(3)
  end

  # GET /posts/1
  def show
  end

  # GET /posts/new
  def new
    @post = Post.new
  end

  # GET /posts/1/edit
  def edit
  end

  # POST /posts
  def create
    @post = Post.new(post_params)
    @post.user_id = current_user.id
    if @post.save
      redirect_to @post, notice: 'Scrippet Saved.'
    else
      render action: 'new'
    end
  end

  # PATCH/PUT /posts/1
  def update
    if @post.update(post_params)
      redirect_to @post, notice: 'Scrippet Saved.'
    else
      render action: 'edit'
    end
  end

  # DELETE /posts/1
  def destroy
    @post.destroy
    redirect_to posts_url, notice: 'Post was successfully destroyed.'
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_post
      @post = Post.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def post_params
      params.require(:post).permit(:title, :body, :publish_date, :user_id, :adult, :terms, :tag_list, category_ids: [])
    end
end
