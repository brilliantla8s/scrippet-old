require 'spec_helper'

describe "tenants/edit" do
  before(:each) do
    @tenant = assign(:tenant, stub_model(Tenant))
  end

  it "renders the edit tenant form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form[action=?][method=?]", tenant_path(@tenant), "post" do
    end
  end
end
